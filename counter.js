
    function animateCount(elem,duration,complete_callback="")
    {

        if(complete_callback == "")
        {
            complete_callback = 'default_complete_callback';
        }
        var count_start = elem.attr('counter-start');
        var count_end = elem.attr('counter-ends');

            elem.prop('Counter',count_start).animate({
                Counter: count_end
            }, {
                duration: duration,
                easing: 'swing',
                step: function (now) {

                    var number = Math.ceil(now);

                    elem.text(number);

                },
                complete:function(){
                    console.log('Timer Finished');
                }
            });

    }

    function default_complete_callback(){

        console.log('Timer Finished');
    }
